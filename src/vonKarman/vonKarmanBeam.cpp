// Sriramajayam

#include <vonKarmanBeam.h>
#include <cassert>
#include <iostream>

// Implementation

// Constructor
vonKarmanBeam::vonKarmanBeam(const int e, const Element* IElm, const vonKarmanMaterial* vm)
  :DResidue(),
   elmnum(e),
   Elm(static_cast<const HermiteElement<2>*>(IElm)),
   vkMat(vm),
   FieldsUsed({0,1}) 
{}


// Copy constructor
vonKarmanBeam::vonKarmanBeam(const vonKarmanBeam& Obj)
  :DResidue(Obj),
   elmnum(Obj.elmnum),
   Elm(Obj.Elm),
   vkMat(Obj.vkMat),
   FieldsUsed(Obj.FieldsUsed) {}

// Destructor
vonKarmanBeam::~vonKarmanBeam() {}

// Cloning
vonKarmanBeam* vonKarmanBeam::Clone() const
{ return new vonKarmanBeam(*this); }

// Returns the fields used
const std::vector<int>& vonKarmanBeam::GetField() const
{ return FieldsUsed; }

// Returns the number of dofs in requested field
int vonKarmanBeam::GetFieldDof(int fieldnumber) const
{ return GetElement()->GetDof(FieldsUsed[fieldnumber]); }


// Returns the vonKarman material object
const vonKarmanMaterial* vonKarmanBeam::GetvonKarmanMaterial() const
{ return vkMat; }

// Returns the element
const HermiteElement<2>* vonKarmanBeam::GetElement() const
{ return Elm; }


// Resize & initialize force and stiffness matrix
void vonKarmanBeam::
ResizeAndInitializeForceAndStiffness(std::vector<std::vector<double>> *funcval, 
				     std::vector<std::vector<std::vector<std::vector<
				     double>>>>* dfuncval) const
{
  const std::vector<int> MyFields = GetField();
  const int nFields = int(MyFields.size());
  std::vector<int> nDof(nFields);
  for(int f=0; f<nFields; f++)
    nDof[f] = GetFieldDof(f);
  
  if( funcval!=nullptr )
    {
      if( int(funcval->size())<nFields )
	funcval->resize( nFields );
      for(int f=0; f<nFields; f++)
	{
	  if( int((*funcval)[f].size())<nDof[f] )
	    (*funcval)[f].resize( nDof[f] );
	  for(int a=0; a<nDof[f]; a++)
	    (*funcval)[f][a] = 0.;
	}
    }

  if( dfuncval!=nullptr )
    {
      if( int(dfuncval->size())<nFields )
	dfuncval->resize( nFields );
      for(int f=0; f<nFields; f++)
	{
	  if( int((*dfuncval)[f].size())<nDof[f] )
	    (*dfuncval)[f].resize( nDof[f] );
	  for(int a=0; a<nDof[f]; a++)
	    {
	      if( int((*dfuncval)[f][a].size())<nFields )
		(*dfuncval)[f][a].resize( nFields );
	      for(int g=0; g<nFields; g++)
		{
		  if( int((*dfuncval)[f][a][g].size())<nDof[g] )
		    (*dfuncval)[f][a][g].resize( nDof[g] );
		  for(int b=0; b<nDof[g]; b++)
		    (*dfuncval)[f][a][g][b] = 0.;
		}
	    }
	}
    }
}


// Computes functional being minimized
void vonKarmanBeam::ComputeFunctional(const void* arg, double &Fval) const
{
  Fval = 0.;

  // Compute contribution from stretching
  double Fval_Stretch = 0.;
  ComputeFunctional_Stretching(arg, Fval_Stretch);
  
  // Compute contribution from bending
  double Fval_Bend = 0.;
  ComputeFunctional_Bending(arg, Fval_Bend);
  
  Fval = Fval_Stretch + Fval_Bend;
  return;
}



// Compute force vector
void vonKarmanBeam::GetVal(const void* arg,
			   std::vector<std::vector<double>> *funcval) const
{ GetDVal(arg, funcval, nullptr);
  return; }




// Compute force vector and stiffness matrix
void vonKarmanBeam::
GetDVal(const void* arg, 
	std::vector<std::vector<double>> *funcval,
	std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const
{
  ResizeAndInitializeForceAndStiffness(funcval, dfuncval);

  if( dfuncval==nullptr )
    { GetVal_Stretching(arg, funcval);
      GetVal_Bending(arg, funcval); }
  else
    { GetDVal_Stretching(arg, funcval, dfuncval);
      GetDVal_Bending(arg, funcval, dfuncval); }

  return;
}


//////////////////  COMPUTE STRETCHING CONTRIBUTIONS ///////////

// Compute functional
void vonKarmanBeam::
ComputeFunctional_Stretching(const void* arg, double &Fval) const
{
  Fval = 0.;

  // Access the configuration 
  assert(arg!=nullptr && "Argument is null");
  const auto* config = static_cast<const vkConfig*>(arg);
  assert(config!=nullptr && "Argument of the wrong type");
  const auto& L2GMap = *(config->first);
  const auto* dofArray = (config->second);

  // This element, fields and quadrature
  const auto& MyFields = GetField();
  const int nFields = static_cast<int>(MyFields.size());
  std::vector<int> nDof(nFields);
  for(int f=0; f<nFields; f++)
    nDof[f] = GetFieldDof(f);
  const auto& Qwts = Elm->GetIntegrationWeights(MyFields[0]);
  const int nquad = static_cast<int>(Qwts.size());
  
  for(int q=0; q<nquad; q++)
    {
      // derivative of in-plane and out-of-plane displacement
      double dU[nFields];
      for(int f=0; f<nFields; ++f)
	{
	  dU[f] = 0.;
	  for(int a=0; a<nDof[f]; ++a)
	    dU[f] += dofArray[L2GMap.Map(f,a,elmnum)]*Elm->GetDShape(MyFields[f], q, a, 0);
	}
      
      // Strain
      double epsilon = dU[0] + 0.5*dU[1]*dU[1];
      
      Fval += 0.5*Qwts[q]*vkMat->StretchingModulus*epsilon*epsilon;
    }
  return;
}
  


void vonKarmanBeam::
GetVal_Stretching(const void* arg, std::vector<std::vector<double>> *funcval) const
{  GetDVal_Stretching(arg, funcval, nullptr);
  return; }


void vonKarmanBeam::GetDVal_Stretching(const void* arg, 
				       std::vector<std::vector<double>> *funcval,
				       std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const
{
  // Access the configuration here
  assert(arg!=nullptr && "Argument is null");
  const auto* config = static_cast<const vkConfig*>(arg);
  assert(config!=nullptr && "Argument of the wrong type");
  const auto& L2GMap = *(config->first);
  const auto* dofArray = (config->second);
  
  
  // Element, fields, quadrature
  const auto& MyFields = GetField();
  const int nFields = int(MyFields.size());
  std::vector<int> nDof(nFields);
  for(int f=0; f<nFields; f++)
    nDof[f] = GetFieldDof(f);
  const std::vector<double> Qwts = Elm->GetIntegrationWeights(MyFields[0]);
  const int nquad = int(Qwts.size());
  
  for(int q=0; q<nquad; q++)
    {
      // derivative of in-plane and out-of-plane displacement
      double dU[nFields];
      for(int f=0; f<nFields; f++)
	{
	  dU[f] = 0.;
	  for(int a=0; a<nDof[f]; a++)
	    dU[f] += dofArray[L2GMap.Map(f,a,elmnum)]*Elm->GetDShape(MyFields[f], q, a, 0);
	}

      // Strain
      double epsilon = dU[0] + 0.5*dU[1]*dU[1];
      
      for(int f=0; f<nFields; f++)
	for(int a=0; a<nDof[f]; a++)
	  {
	    // Variation of displacement gradient in the direction Na e_f:
	    double delta_FA_dU[nFields];
	    for(int i=0; i<nFields; i++)
	      delta_FA_dU[i] = 0.;
	    delta_FA_dU[f] = Elm->GetDShape(MyFields[f], q, a, 0);
	    
	    // Variation of strain in the direction Na e_f:
	    double delta_FA_epsilon = 
	      delta_FA_dU[0] + dU[1]*delta_FA_dU[1];
	    
	    (*funcval)[f][a] += 
	      Qwts[q]*vkMat->StretchingModulus*epsilon*delta_FA_epsilon;
	    
	    if( dfuncval!=nullptr )
	      for(int g=0; g<nFields; g++)
		for(int b=0; b<nDof[g]; b++)
		  {
		    // Variation of displacement derivatives in the direction Nb e_g:
		    double delta_GB_dU[nFields];
		    for(int i=0; i<nFields; i++)
		      delta_GB_dU[i] = 0.;
		    delta_GB_dU[g] = Elm->GetDShape(MyFields[g], q, b, 0);
		    
		    // Variation of strain in the direction Nb e_g:
		    const double delta_GB_epsilon = 
		      delta_GB_dU[0] + dU[1]*delta_GB_dU[1];
		    
		    // Variation of delta_FA_epsilon in the direction Nb e_g:
		    const double delta_GB_delta_FA_epsilon = 
		      delta_FA_dU[1]*delta_GB_dU[1];

		    (*dfuncval)[f][a][g][b] += Qwts[q]*vkMat->StretchingModulus*
		      ( delta_FA_epsilon*delta_GB_epsilon + 
			epsilon*delta_GB_delta_FA_epsilon );
		  }
	  }
    }

  return;
}


//////// END COMPUTATION OF STRETCHING TERMS //////////



//////// BEGIN COMPUTATION OF BENDING TERMS ////////	
	    
void vonKarmanBeam::
ComputeFunctional_Bending(const void* arg, double &Fval) const
{
  Fval = 0.;
  
  // Access the configuration here
  assert(arg!=nullptr && "Argument is null");
  const auto* config = static_cast<const vkConfig*>(arg);
  assert(config!=nullptr && "Argument of the wrong type");
  const auto& L2GMap = *(config->first);
  const auto* dofArray = (config->second);
  
  const int wfield =  1;
  const int nDof = GetFieldDof(wfield);
  const auto& Qwts = Elm->GetIntegrationWeights(wfield);
  const int nquad = static_cast<int>(Qwts.size());
  
  for(int q=0; q<nquad; q++)
    {
      // 2nd derivative of out-of-plane displacement
      double d2W = 0.;
      for(int a=0; a<nDof; a++)
	d2W += dofArray[L2GMap.Map(wfield,a,elmnum)]*Elm->GetD2Shape(wfield, q, a);
      
      Fval += 0.5*Qwts[q]*vkMat->BendingModulus*d2W*d2W;
    }

  return;
}



void vonKarmanBeam::
GetVal_Bending(const void* arg,
	       std::vector< std::vector<double> > *funcval) const
{  GetDVal_Bending(arg, funcval, nullptr);
  return; }


void vonKarmanBeam::
GetDVal_Bending(const void* arg,
		std::vector<std::vector<double>> *funcval,
		std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const
{
  // Access the configuration here
  assert(arg!=nullptr && "Argument is null");
  const auto* config = static_cast<const vkConfig*>(arg);
  assert(config!=nullptr && "Argument of the wrong type");
  const auto& L2GMap = *(config->first);
  const auto* dofArray = (config->second);

  // This element, fields and quadrature
  const int wfield =  1;
  const int nDof = GetFieldDof(wfield);
  const auto& Qwts = Elm->GetIntegrationWeights(wfield);
  const int nquad = static_cast<int>(Qwts.size());
  
  for(int q=0; q<nquad; q++)
    {
      // 2nd derivative of out-of-plane displacement
      double d2W = 0.;
      for(int a=0; a<nDof; a++)
	d2W += dofArray[L2GMap.Map(wfield,a,elmnum)]*Elm->GetD2Shape(wfield, q, a);
      
      for(int a=0; a<nDof; a++)
	{
	  (*funcval)[wfield][a] += 
	    Qwts[q]*vkMat->BendingModulus*d2W*Elm->GetD2Shape(wfield, q, a);
	  
	  if( dfuncval!=nullptr )
	    for(int b=0; b<nDof; b++)
	      (*dfuncval)[wfield][a][wfield][b] += Qwts[q]*vkMat->BendingModulus*
		Elm->GetD2Shape(wfield, q, a)*
		Elm->GetD2Shape(wfield, q, b);
	}
    }
  
  return;
}


// Consistency test
bool vonKarmanBeam::ConsistencyTest(const void* arg,
				    const double pertEPS,
				    const double tolEPS) const
{
  // Access this configuration
  assert(arg!=nullptr && "Argument is null");
  const auto* config = static_cast<const vkConfig*>(arg);
  assert(config!=nullptr && "Argument of the wrong type");
  const auto& L2GMap = *(config->first);
  const auto* dofArray = (config->second);
  const int nTotalDof = L2GMap.GetTotalNumDof();

  // Dofs for fields (all assumed to be the same)
  const int nFields = 2;
  const int nDof = GetFieldDof(0);

  // Residuals & stiffness
  std::vector<std::vector<double>> res(nFields), resPlus(nFields), resMinus(nFields);
  std::vector<std::vector<std::vector<std::vector<double>>>> dres(nFields);
  ResizeAndInitializeForceAndStiffness(&res, &dres);
  ResizeAndInitializeForceAndStiffness(&resPlus, nullptr);
  ResizeAndInitializeForceAndStiffness(&resMinus, nullptr);

  // Correct residual and stiffness
  GetDVal(arg, &res, &dres);

  // Approx. residual and stiffness
  double funcPlus, funcMinus;
  std::vector<double> dofPert(nTotalDof);
  std::copy(dofArray, dofArray+nTotalDof, dofPert.begin());
  
  for(int f=0; f<nFields; ++f)
    for(int a=0; a<nDof; ++a)
      {
	// Perturbed configuration
	vkConfig pertConfig(&L2GMap, &dofPert[0]);
	
	// Positive perturbation
	dofPert[L2GMap.Map(f,a,elmnum)] += pertEPS;
	ComputeFunctional(&pertConfig, funcPlus);
	GetVal(&pertConfig, &resPlus);

	// Negative perturbation
	dofPert[L2GMap.Map(f,a,elmnum)] -= 2.*pertEPS;
	ComputeFunctional(&pertConfig, funcMinus);
	GetVal(&pertConfig, &resMinus);

	// Restore perturbations
	dofPert[L2GMap.Map(f,a,elmnum)] += pertEPS;
	
	// Check consistency
	double resnum = (funcPlus-funcMinus)/(2.*pertEPS);
	assert(std::abs(res[f][a]-resnum)<tolEPS && "Residual is inconsistent");
	for(int g=0; g<nFields; ++g)
	  for(int b=0; b<nDof; ++b)
	    {
	      double dresnum = (resPlus[g][b]-resMinus[g][b])/(2.*pertEPS);
	      assert(std::abs(dresnum-dres[f][a][g][b])<tolEPS && "Stiffness is inconsistent");
	    }
      }
  return true;
}
