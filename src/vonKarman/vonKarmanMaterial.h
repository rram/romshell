// Sriramajayam

#ifndef VONKARMANMATERIAL
#define VONKARMANMATERIAL

class vonKarmanMaterial
{
 public:
  double BendingModulus;
  double StretchingModulus;
  double PoissonRatio;
  vonKarmanMaterial(const double bm, const double sm, const double pr)
    :BendingModulus(bm),
    StretchingModulus(sm),
    PoissonRatio(pr) {}
};


#endif
