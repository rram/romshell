// Sriramajayam

#include <vonKarmanBeam.h>
#include <L2GMapForHermiteElement.h>
#include <Assembler.h>
#include <PetscData.h>
#include <fstream>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a 1D mesh over [0,0.5]
  const int nNodes = 50;
  const double h = 0.5/static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int i=0; i<nNodes; ++i)
    coordinates[i] = static_cast<double>(i)*h;
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }

  // 1D hermite elements
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::vector<Element*> ElmArray(nElements);
  for(int e=0; e<nElements; ++e)
    {
      const int* conn = &connectivity[2*e];
      ElmArray[e] = new HermiteElement<2>(conn[0], conn[1]);
    }
  L2GMapForHermiteElement L2GMap(ElmArray);
  const int nTotalDofs = L2GMap.GetTotalNumDof();
  
  // Material
  const double breath = 20.*1.e-3;
  const double thickness = 2.98*1.e-3;
  // divide all constants by E*h
  vonKarmanMaterial vkMat({.BendingModulus=thickness*thickness/12.,
	.StretchingModulus=1.,
	.PoissonRatio=0.});

  // Operations
  std::vector<vonKarmanBeam*> OpArray(nElements);
  for(int e=0; e<nElements; ++e)
    OpArray[e] = new vonKarmanBeam(e, ElmArray[e], &vkMat);

  // Assembler
  StandardAssembler<vonKarmanBeam> Asm(OpArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);
  
  // Initialize PETSc Data
  PetscData PD;
  PD.Initialize(nz);

  // Boundary conditions (0,2)|-------|(1,3)
  std::vector<double> bvalues({});
  std::vector<int> boundary({});
  // Left end clamped
  for(int f=0; f<2; ++f)
    { boundary.push_back( L2GMap.Map(f,0,0) );
      bvalues.push_back(0.);
      boundary.push_back( L2GMap.Map(f,2,0) );
      bvalues.push_back(0.); }
  // Right end is displaced
  {
    boundary.push_back( L2GMap.Map(0,1,nElements-1) ); // xdisp
    bvalues.push_back( -0.066 );
    boundary.push_back( L2GMap.Map(1,1,nElements-1) ); // y disp
    bvalues.push_back( 0.224 );
  }

  // Dof values
  std::vector<double> dofVals(nTotalDofs);
  std::fill(dofVals.begin(), dofVals.end(), 0.);
  
  // Set prescribed displacement values
  const int nbcs = static_cast<int>(boundary.size());
  for(int i=0; i<nbcs; ++i)
    dofVals[boundary[i]] = bvalues[i];
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Newton-Raphson
  VecZeroEntries(PD.solutionVEC);
  while(true)
    {
      // Update the displacement map
      double* deltaDisp;
      PetscErrorCode ierr;
      ierr = VecScale(PD.solutionVEC, -1.); CHKERRQ(ierr);
      ierr = VecGetArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);
      for(int a=0; a<nTotalDofs; ++a)
	dofVals[a] += deltaDisp[a];
      ierr = VecRestoreArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);


      // Next update
      vkConfig config(&L2GMap, &dofVals[0]);
      Asm.Assemble(&config, PD.resVEC, PD.stiffnessMAT);
      PD.SetDirichletBCs(boundary, bvalues);
      PD.Solve();
      if(PD.HasConverged(1.e-10, 1.,1.))
	{ std::cout<<"\nConverged! "<<std::flush; 
	  break; }
    }

  // Plot the undeformed and deformed meshes
  std::fstream pfile;
  pfile.open((char*)"disp.dat", std::ios::out);
  for(int n=0; n<nNodes-1; ++n)
    { int xdof = L2GMap.Map(0,0,n);
      int ydof = L2GMap.Map(1,0,n);
      pfile<<coordinates[n]<<" 0. "<<coordinates[n]+dofVals[xdof]<<" "<<dofVals[ydof]<<"\n"; }
  // Last node
  { int xdof = L2GMap.Map(0,1,nElements-1);
    int ydof = L2GMap.Map(1,1,nElements-1);
    pfile<<coordinates[nNodes-1]<<" 0. "<<coordinates[nNodes-1]+dofVals[xdof]<<" "<<dofVals[ydof]<<"\n"; }
  
  pfile.flush();
  pfile.close();
  
  for(auto& x:ElmArray) delete x;
  for(auto& x:OpArray) delete x;
  PD.Destroy();
  PetscFinalize();
}
