// Sriramajayam

#include <HermiteElement.h>
#include <L2GMapForHermiteElement.h>
#include <vonKarmanBeam.h>
#include <iostream>
#include <cassert>
#include <random>

void Test(const vonKarmanBeam& Op, const LocalToGlobalMap& L2GMap);

int main()
{
  // Create 1D element
  std::vector<double> coordinates({std::sqrt(2.), std::sqrt(5.)});
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  
  // Create elements
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new HermiteElement<2>(1,2);
  
  // Local to global map
  L2GMapForHermiteElement L2GMap(ElmArray);

  // Material
  vonKarmanMaterial vkMat({
      .BendingModulus=std::sqrt(15.),
	.StretchingModulus=std::sqrt(20),
	.PoissonRatio=0.3});

  // Operation
  const int elmNum = 0;
  vonKarmanBeam Op(elmNum, ElmArray[0], &vkMat);
  Test(Op, L2GMap);

  delete ElmArray[0];
}


void Test(const vonKarmanBeam& Op, const LocalToGlobalMap& L2GMap)
{
  assert(Op.GetField()==std::vector<int>({0,1}));
  assert(Op.GetFieldDof(0)==4);
  assert(Op.GetFieldDof(1)==4);
  assert(std::abs(Op.GetvonKarmanMaterial()->BendingModulus-std::sqrt(15.))<1.e-4);
  assert(std::abs(Op.GetvonKarmanMaterial()->StretchingModulus-std::sqrt(20.))<1.e-4);
  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<double> dofs(nTotalDof);

  // Create configuration
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1.,1.);
  for(int a=0; a<nTotalDof; ++a)
    dofs[a] = dis(gen);
  vkConfig config(&L2GMap, &dofs[0]);
  
  // Consistency test
  assert(Op.ConsistencyTest(&config, 1.e-5, 1.e-4)==true);

  return;
}
