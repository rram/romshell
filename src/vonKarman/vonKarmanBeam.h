// Sriramajayam

#ifndef VONKARMANBEAM
#define VONKARMANBEAM

#include <vonKarmanMaterial.h>
#include <ElementalOperation.h>
#include <HermiteElement.h>
#include <LocalToGlobalMap.h>

// A configuration is specified using:
// (i)  the local to global map
// (ii) and the global dof array
using vkConfig = std::pair<const LocalToGlobalMap*, const double*>;

class vonKarmanBeam: public DResidue
{
 public:
  //! Constructor
  //! \param[in] e Element number
  //! \param IElm ELement object, should be typecastable to HermiteElement<2>
  //! \param vm vonKarman material object
  //! Assumes that displacement fields are 0, 1 with 1 corresponding to the bending direction
  vonKarmanBeam(const int e,
		const Element* IElm, const vonKarmanMaterial* vm); 
  
  //! Copy constructor
  //! \param Obj Object to be copied
  vonKarmanBeam(const vonKarmanBeam& Obj);
  
  //! Destructor
  virtual ~vonKarmanBeam();
  
  //! Cloning
  virtual vonKarmanBeam* Clone() const override;
  
  //! Returns the fields used
  const std::vector<int>& GetField() const override;
  
  //! Returns the number of dofs in requested field
  //! \param fieldnumber Local field number
  int GetFieldDof(int fieldnumber) const override;
  
  //! Returns the vonKarman material object
  const vonKarmanMaterial* GetvonKarmanMaterial() const;
  
  //! Returns the elemnt
  const HermiteElement<2>* GetElement() const;
  
  //! Computes the von-Karman functional
  //! \param[in] argval Values of dofs for each field
  //! \param[out] Fval Computed functional
  void ComputeFunctional(const void* arg, double &Fval) const;
  
  //! Computes the residue (element force vector)
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  void GetVal(const void* arg,
		      std::vector< std::vector<double> > *funcval) const override;
  
  //! Computes the element stiffness matrix and force vector
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  //! \param[out] dfuncval Computed element stiffness matrix
  void GetDVal(const void* arg, 
	       std::vector<std::vector<double>> *funcval,
	       std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const override;

   // Consistency test
  //! \param[in] argval Configuration at which to perform the consistency test
  //! \param[in] pertEPS Tolerance to use for perturbation
  //! \param[in] pertEPS Tolerance to use for consistency check
  bool ConsistencyTest(const void* argval,
		       const double pertEPS,
		       const double tolEPS) const override;
  
 protected:
  //! Resize & initialize force and stiffness matrix
  void ResizeAndInitializeForceAndStiffness
    (std::vector<std::vector<double>> *funcval, 
     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const;
 
  
  //! Computes the stretching contribution
  //! \param[in] argval Values of dofs for each field
  //! \param[out] Fval Computed functional
  void ComputeFunctional_Stretching(const void* arg, double &Fval) const;
  
  //! Computes the residue (element force vector)
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  void GetVal_Stretching(const void* arg,
			 std::vector<std::vector<double>> *funcval) const;
  
  //! Computes the element stiffness matrix and force vector
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  //! \param[out] dfuncval Computed element stiffness matrix
  void GetDVal_Stretching(const void* arg, 
			  std::vector<std::vector<double> > *funcval,
			  std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const;


  //! Computes the bending contribution
  //! \param[in] argval Values of dofs for each field
  //! \param[out] Fval Computed functional
  void ComputeFunctional_Bending(const void* arg, double &Fval) const;
  
  //! Computes the residue (element force vector)
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  void GetVal_Bending(const void* arg, 
		      std::vector<std::vector<double>> *funcval) const;
  
  //! Computes the element stiffness matrix and force vector
  //! \param[in] argval Values of dofs for each field
  //! \param[out] funcval Computed element force vector
  //! \param[out] dfuncval Computed element stiffness matrix
  void GetDVal_Bending(const void* arg, 
		       std::vector<std::vector<double>> *funcval,
		       std::vector<std::vector<std::vector<std::vector<double>>>> *dfuncval) const;


 private:
  const int elmnum;
  const HermiteElement<2>* Elm;
  const vonKarmanMaterial* vkMat;
  std::vector<int> FieldsUsed;
};


#endif
