// Sriramajayam

#ifndef BSPLINE_1D_SHAPE_IMPL_H
#define BSPLINE_1D_SHAPE_IMPL_H

#include <BSpline1DShape.h>

// Implementation of the class

// Default constructor
template< int DEGREE>
BSpline1DShape<DEGREE>::BSpline1DShape() {}

// Constructor
// GSL computes knots from a given set of break points by repeating 
// break points at the ends.
// We will not do so here. Instead, we will directly set the knot vector
// that has been provided.
// The only place where the break points terminolofy is useful is to 
// set the correct size of data structures when allocating workspaces


// FYI: From GSL's b spline documentation
/*
 *  gsl_bspline_knots()
 *  Compute the knots from the given breakpoints:
 *
 *  knots(1:k) = breakpts(1) 
 *  knots(k+1:k+l-1) = breakpts(i), i = 2 .. l
 * knots(n+1:n+k) = breakpts(l + 1)
 *  
 *  where l is the number of polynomial pieces (l = nbreak - 1) and
 *  n = k + l - 1
 *  (using matlab syntax for the arrays)
 *  
 *  The repeated knots at the beginning and end of the interval
 *  correspond to the continuity condition there. See pg. 119
 *  of [1]
 *  Inputs: breakpts - breakpoints
 *  w        - bspline workspace
 *  
 * Return: success or error
 */
// Constructor
template< int DEGREE>
BSpline1DShape<DEGREE>::BSpline1DShape(const std::vector<double>& kv)
:Shape()
{ InitializeBSpline(kv); }


// Initialize Bspline
template< int DEGREE>
void BSpline1DShape<DEGREE>::InitializeBSpline(const std::vector<double>& kv)
{
  // Number of knots
  const int nknots = int(kv.size());
  
  // Order of the spline
  const int order = DEGREE+1;
  
  // Number of control points (eg: 4 fewer than the number of knots for a cubic spline)
  const int ncontrol = nknots-order;
  
  // Number of break points (convention used by GSL)
  const int nbreak = ncontrol-order+2;
  
  // Allocate workspace for bsplines
  bw = gsl_bspline_alloc(order, nbreak);
  
  // Set the knot vector for the workspace
  for(int k=0; k<nknots; ++k)
    gsl_vector_set(bw->knots, k, kv[k]);
}


// Destructor
template< int DEGREE>
BSpline1DShape<DEGREE>::~BSpline1DShape()
{
  // Free allocated memory
  gsl_bspline_free(bw);
}


// Copy constructor
template< int DEGREE>
BSpline1DShape<DEGREE>::BSpline1DShape(const BSpline1DShape<DEGREE> &Obj)
:Shape(Obj)
{
  // Allocate memory for workspaces
  bw = gsl_bspline_alloc( Obj.GetOrderOfSpline(), Obj.GetNumberOfBreakPoints() );
  
  // Copy the knot vector
  int nknots = Obj.GetNumberOfKnots();
  for(int k=0; k<nknots; ++k)
    gsl_vector_set( bw->knots, k, Obj.GetKnot(k) );
}


// Cloning
template<int DEGREE>
BSpline1DShape<DEGREE>* BSpline1DShape<DEGREE>::Clone() const
{ return new BSpline1DShape<DEGREE>(*this); }


// Return the number of knots
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfKnots() const
{ return bw->knots->size; }


// Returns the requested knot
template<int DEGREE>
double BSpline1DShape<DEGREE>::GetKnot(const int k) const
{ return gsl_vector_get(bw->knots, k); }


// Returns the knot vector
template<int DEGREE>
std::vector<double> BSpline1DShape<DEGREE>::GetKnotVector() const
{
  const int nknots = GetNumberOfKnots();
  std::vector<double> kv(nknots);
  for( int i=0; i<nknots; ++i)
    kv[i] = GetKnot(i);
  return kv;
}


// Returns the degree of spline
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetDegreeOfSpline() const
{ return DEGREE; }

// Returns the number of break points
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfBreakPoints() const
{ return bw->nbreak; }


// Returns the order of the spline
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetOrderOfSpline() const
{ return bw->k; }


// Returns the number of (basis) shape functions
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfFunctions() const
{ return bw->n; }

// Returns the number of basis functions per interval
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfFunctionsPerInterval() const
{ return GetOrderOfSpline(); }

template<int DEGREE>
int BSpline1DShape<DEGREE>::
GetNumberOfFunctionsInInterval(const  int intnum) const
{ return GetNumberOfFunctionsPerInterval(); }

// Returns the number of pieces (elements)
template<int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfIntervals() const
{ return bw->l; }


// Returns the limits of a requested interval
template<int DEGREE>
std::vector<double> BSpline1DShape<DEGREE>::GetInterval(const int intnum) const
{
  std::vector<double> tinterval(2);
  tinterval[0] = GetKnot( intnum+DEGREE );
  tinterval[1] = GetKnot( intnum+DEGREE+1 );
  return tinterval;
}

// Returns the knot numbers constituting the given interval
template<int DEGREE>
std::vector< int> BSpline1DShape<DEGREE>::
GetIntervalKnotNumbers(const int intnum) const
{ 
  std::vector< int> knotnum(2);
  knotnum[0] = intnum+DEGREE;
  knotnum[1] = intnum+DEGREE+1;
  return knotnum;
}

// Returns the indices of shape functions active on a given interval
template<int DEGREE>
std::vector<int> BSpline1DShape<DEGREE>::
GetActiveShapeFunctions(const int intnum) const
{ 
  std::vector<int> activeshapes(DEGREE+1);
  for( int j=0; j<=DEGREE; j++)
    activeshapes[j] = intnum+j;
  return activeshapes;
}

// Returns the number of parametric variables
template< int DEGREE>
int BSpline1DShape<DEGREE>::GetNumberOfVariables() const
{ return 1; }


// Evaluate shape functions
template< int DEGREE>
double BSpline1DShape<DEGREE>::Val(int a, const double *x) const
{
  const size_t shpnum = static_cast<size_t>(a);
  size_t istart, iend;
  gsl_vector* nz_shp = gsl_vector_calloc ( GetOrderOfSpline() );
  
  // Evaluate only nonzero shape functions at this point
  gsl_bspline_eval_nonzero(*x, nz_shp, &istart, &iend, bw);
  
  double val = 0.;
  if( shpnum>=istart && shpnum<=iend )
    val = gsl_vector_get(nz_shp, shpnum-istart);
  
  gsl_vector_free(nz_shp);
  
  return val;
}    


// Evaluate nonzero shape functions at given parametric coordinate
template<int DEGREE>
void BSpline1DShape<DEGREE>::GetNonzeroShapes(const double xi, 
					      std::vector< int>& shpnum, 
					      std::vector<double>& vals) const
{
  shpnum.clear();
  vals.clear();
  
  // Evaluate nonzero shape functions
  size_t istart, iend;
  gsl_vector* nz_shp = gsl_vector_calloc( GetOrderOfSpline() );
  gsl_bspline_eval_nonzero(xi, nz_shp, &istart, &iend, bw);
  for(size_t i=istart; i<=iend; i++)
    {
      shpnum.push_back( i );
      vals.push_back( gsl_vector_get(nz_shp, i-istart) );
    }
  gsl_vector_free(nz_shp);
  //assert(bw->k==static_cast<int>(shpnum.size()));
  return;
}
					      
  

// Evaluate derivative of shape functions
template< int DEGREE>
double BSpline1DShape<DEGREE>::DVal(int a, const double *x, int i) const
{
  const size_t shpnum = static_cast<size_t>(a);
  gsl_matrix* nz_dshp = gsl_matrix_calloc( GetOrderOfSpline(), 2);
  size_t istart, iend;
  
  // Evaluate only nonzero shape functions at this point
  gsl_bspline_deriv_eval_nonzero(*x, 1, nz_dshp, &istart, &iend, bw);
  
  double dval = 0.;
  if( shpnum>=istart && shpnum<=iend )
    dval = gsl_matrix_get(nz_dshp, shpnum-istart, 1);
  
  gsl_matrix_free(nz_dshp);
 
  return dval;
}


// Evaluate derivatives of nonzero shape functions at given parametric coordinate
template< int DEGREE>
void BSpline1DShape<DEGREE>::GetNonzeroDShapes(const double xi, 
					       std::vector< int>& shpnum, 
					       std::vector<double>& dvals) const
{
  shpnum.clear();
  dvals.clear();
  
  // Evaluate derivatives of nonzero shape functions
  size_t istart, iend;
  gsl_matrix* nz_dshp = gsl_matrix_calloc( GetOrderOfSpline(), 2 );
  gsl_bspline_deriv_eval_nonzero(xi, 1, nz_dshp, &istart, &iend, bw);
  for(size_t i=istart; i<=iend; i++)
    {
      shpnum.push_back( i );
      dvals.push_back( gsl_matrix_get(nz_dshp, i-istart, 1) );
    }
  gsl_matrix_free(nz_dshp);
  //assert(bw->k==static_cast<int>(shpnum.size()));
  return;
}

// Evaluate the second derivative of shape functions
template< int DEGREE>
double BSpline1DShape<DEGREE>::D2Val(int a, const double *x, int i, int j) const
{ 
  if( DEGREE<=1 )
    return 0.;
  
  const size_t shpnum = static_cast<size_t>(a);
  
  gsl_matrix* nz_d2shp = gsl_matrix_calloc( GetOrderOfSpline(), 3);
  size_t istart, iend;
  
  // Evaluate only nonzero shape functions at this point
  gsl_bspline_deriv_eval_nonzero(*x, 2, nz_d2shp, &istart, &iend, bw);
  
  double d2val = 0.;
  if( shpnum>=istart && shpnum<=iend )
    d2val = gsl_matrix_get(nz_d2shp, shpnum-istart, 2);
  
  gsl_matrix_free(nz_d2shp);
  
  return d2val;
}


// Evaluate 2nd-derivatives of nonzero shape functions at given parametric coordinate
template< int DEGREE>
void BSpline1DShape<DEGREE>::GetNonzeroD2Shapes(const double xi, 
						std::vector<int>& shpnum, 
						std::vector<double>& d2vals) const
{
  shpnum.clear();
  d2vals.clear();
  
  if( DEGREE<=1 ) return;
  
  // Evaluate derivatives of nonzero shape functions
  size_t istart, iend;
  gsl_matrix* nz_d2shp = gsl_matrix_calloc( GetOrderOfSpline(), 3 );
  gsl_bspline_deriv_eval_nonzero(xi, 2, nz_d2shp, &istart, &iend, bw);
  for(size_t i=istart; i<=iend; i++)
    {
      shpnum.push_back( i );
      d2vals.push_back( gsl_matrix_get(nz_d2shp, i-istart, 2) );
    }
  gsl_matrix_free(nz_d2shp);
  //assert(bw->k==static_cast<int>(shpnum.size()));
  return;
}

#endif
