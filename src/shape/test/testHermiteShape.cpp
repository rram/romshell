// Sriramajayam

#include <HermiteShape.h>
#include <random>
#include <iomanip>
#include <fstream>
#include <cassert>

void Test(const HermiteShape& HP, const double Jac);

int main()
{
  // Testing Hermite Polynomials.
  double endpoints[2] = {std::sqrt(2.), std::sqrt(5.)};
  const double Jac = endpoints[0]-endpoints[1];
  HermiteShape HP(endpoints);

  Test(HP, Jac);
}


void Test(const HermiteShape& HP, const double Jac)
{
  assert(HP.GetNumberOfFunctions()==4);
  assert(HP.GetNumberOfVariables()==1);

  // Test shape function values
  const double EPS = 1.e-5;
  double lambda[1];
  lambda[0] = 1.;
  assert(std::abs(HP.Val(0,lambda)-1.)<EPS &&
	 std::abs(HP.Val(1,lambda))<EPS &&
	 std::abs(HP.Val(2,lambda))<EPS &&
	 std::abs(HP.Val(3,lambda))<EPS);

  lambda[0] = 0.;
  assert(std::abs(HP.Val(0,lambda))<EPS &&
	 std::abs(HP.Val(1,lambda)-1.)<EPS &&
	 std::abs(HP.Val(2,lambda))<EPS &&
	 std::abs(HP.Val(3,lambda))<EPS);
  
  lambda[0] = 1.;
  assert(std::abs(HP.DVal(0,lambda,0))<EPS &&
	 std::abs(HP.DVal(1,lambda,0))<EPS &&
	 std::abs(HP.DVal(2,lambda,0)/Jac-1.)<EPS &&
	 std::abs(HP.DVal(3,lambda,0))<EPS);

  lambda[0] = 0.;
  assert(std::abs(HP.DVal(0,lambda,0))<EPS &&
	 std::abs(HP.DVal(1,lambda,0))<EPS &&
	 std::abs(HP.DVal(2,lambda,0))<EPS &&
	 std::abs(HP.DVal(3,lambda,0)/Jac-1.)<EPS);

  // Random points
  std::random_device rd;  
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,1.);
  for(int p=0; p<100; ++p)
    {
      lambda[0] = dis(gen);

      // Partition of unity
      double sum = HP.Val(0,lambda) + HP.Val(1,lambda);
      assert(std::abs(sum-1.)<EPS);
      double dsum = HP.DVal(0,lambda,0) + HP.DVal(1,lambda,0);
      assert(std::abs(dsum)<EPS);

      // Consistency of first derivatives
      for(int a=0; a<4; ++a)
	{
	  double dval = HP.DVal(a,lambda,0);
	  lambda[0] += EPS;
	  double vplus = HP.Val(a,lambda);
	  lambda[0] -= EPS;
	  double vminus = HP.Val(a,lambda);
	  lambda[0] += EPS;
	  double dnum = (vplus-vminus)/(EPS);
	  assert(std::abs(dval-dnum)<10.*EPS);
	}

      // Consistency of second derivatives
      for(int a=0; a<4; ++a)
	{
	  double dval = HP.D2Val(a,lambda);
	  lambda[0] += EPS;
	  double vplus = HP.DVal(a,lambda,0);
	  lambda[0] -= EPS;
	  double vminus = HP.DVal(a,lambda,0);
	  lambda[0] += EPS;
	  double dnum = (vplus-vminus)/(EPS);
	  assert(std::abs(dval-dnum)<10.*EPS);
	}
      
    }
}
  
