// Sriramajayam

#include "BSpline1DShape.h"
#include <iostream>
#include <fstream>

void PrintShapeDetails(const CubicBSpline1DShape &Shp,
		       const char *filename=0);

int main()
{
  int nknots = 10;
  std::vector<double> knots; knots.clear();
  for(int k=0; k<nknots; k++)
    knots.push_back( double(k) );
  
  CubicBSpline1DShape *Clone;
  {
    std::cout<<"\n\nTesting object: "
	     <<"\n===================";
    CubicBSpline1DShape Shp(knots);
    PrintShapeDetails(Shp, (char*)"Shp");
    
    std::cout<<"\n\nTesting copy:     "
	     <<"\n===================";
    CubicBSpline1DShape Copy(Shp);
    PrintShapeDetails(Copy);
    
    Clone = Shp.Clone();
  }
  
  std::cout<<"\n\nTesting clone:     "
	     <<"\n===================";
  PrintShapeDetails(*Clone);
    
  std::cout<<"\n\nDONE\n\n";
}


void PrintShapeDetails(const CubicBSpline1DShape &Shp, const char* filename)
{
  std::cout<<"\nNumber of knots: "<<Shp.GetNumberOfKnots();
  std::cout<<"\nKnot vector: ";
  for(int k=0; k<Shp.GetNumberOfKnots(); k++)
    std::cout<<Shp.GetKnot(k)<<", ";
  std::cout<<"\nDegree of spline: "<<Shp.GetDegreeOfSpline();
  std::cout<<"\nTotal number of functions: "<<Shp.GetNumberOfFunctions();
  std::cout<<"\nNumber of functions per interval: "<<Shp.GetNumberOfFunctionsPerInterval();
  std::cout<<"\nNumber of intervals: "<<Shp.GetNumberOfIntervals();
  std::cout<<"\nLimits of interval: ("
	   <<Shp.GetInterval(0)[0]<<","<<Shp.GetInterval(0)[1]<<")";
  std::cout<<"\nNumber of active shape functions in interval: "
	   <<Shp.GetActiveShapeFunctions(0).size();
  std::cout<<"\nActive shape function numbers in interval: "
	   <<Shp.GetActiveShapeFunctions(0)[0]<<", "
	   <<Shp.GetActiveShapeFunctions(0)[1]<<", "
	   <<Shp.GetActiveShapeFunctions(0)[2]<<", "
	   <<Shp.GetActiveShapeFunctions(0)[3];
  std::cout<<"\nNumber of variables: "<<Shp.GetNumberOfVariables();
  
  // Interval in which all basis functions are nonzero
  double tmin = Shp.GetKnot(3);
  double tmax = Shp.GetKnot( Shp.GetNumberOfFunctions() );
  std::cout<<"\nInterval of element: "<<tmin<<", "<<tmax;
  
  std::cout<<"\nTesting ";
  if( filename!=NULL )
    std::cout<<"and printing ";
  std::cout<<"values, derivatives and second derivatives: ";
  for(int a=0; a<Shp.GetNumberOfFunctions(); a++)
    {
      char myfilename[100];
      std::fstream sfile;
      if( filename!=NULL )
	{
	  sprintf(myfilename, "%s-%d.dat", filename, a);
	  sfile.open(myfilename, std::ios::out);
	}
      const int N = 50;
      for(int i=1; i<N; i++)
	{
	  double tval = tmin + (tmax-tmin)*double(i)/double(N);
	  double val = Shp.Val(a, &tval);
	  double dval = Shp.DVal(a, &tval, 0);
	  double d2val = Shp.D2Val(a, &tval, 0, 0);
	  if( filename!=NULL )
	    sfile<<tval<<", "<<val<<", "<<dval<<", "<<d2val<<"\n";
	  
	  // Test numerical derivatives
	  double eps = 1.e-4;
	  double tplus = tval+eps;
	  double fplus = Shp.Val(a, &tplus);
	  double tminus = tval-eps;
	  double fminus = Shp.Val(a, &tminus);
	  double dvalnum  = (fplus-fminus)/(2.*eps);
	  double d2valnum = (fplus-2.*val+fminus)/( eps*eps );
	  if( fabs(dvalnum-dval) > 10.*eps )
	    {
	      std::cout<<"\nCould not check numerical derivative: "
		       <<dvalnum<<" different from "<<dval;
	      std::fflush( stdout );
	      exit(1);
	    }
	  if( fabs(d2valnum-d2val) > 10.*eps )
	    {
	      std::cout<<"\nCould not check numerical second derivative: "
		       <<d2valnum<<" different from "<<d2val;
	      std::fflush( stdout );
	      exit(1);
	    }
	}
      if( filename!=NULL )
	sfile.close();
    }
  std::cout<<"complete";
  
  std::cout<<"\nChecking partition of unity property: ";
  {
    std::vector<double> tinterval = Shp.GetInterval(0);
    const int N = 100;
    for(int i=1; i<N; i++)
      {
	double tval = tinterval[0] + (tinterval[1]-tinterval[0])*double(i)/double(N);
	double sum = 0;
	for(int a=0; a<Shp.GetNumberOfFunctionsPerInterval(); a++)
	  sum += Shp.Val(a, &tval);
	if( fabs(sum-1.)>1.e-6 )
	  {
	    std::cerr<<"FAILED";
	    std::fflush( stdout );
	    exit(1);
	  }
      }
    std::cout<<"VERIFIED";
  }
  
}
