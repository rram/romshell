// Sriramajayam

#include <HermiteShape.h>
#include <cmath>
#include <cassert>

extern "C" void dgesv_(int *, int *, double *, int *, int *, double *, int *, int *);

//! Constructor
HermiteShape::HermiteShape(const double * endpts)
  :Shape(),
   elmcoord{endpts[0], endpts[1]}
{
  // Use petsc to invert a matrix to find the coordinates
  double A[16] = {1, endpts[0], std::pow(endpts[0],2.), std::pow(endpts[0],3.),   // for value at left end
		  1, endpts[1], std::pow(endpts[1],2.), std::pow(endpts[1],3.),   // for value at right end
		  0., 1., 2.*endpts[0], 3.*std::pow(endpts[0],2.),           // for value of derivative at left
		  0., 1., 2.*endpts[1], 3.*std::pow(endpts[1],2.) };
      
  // Invert Matrix A:
  double RHS[16] = {1.,0.,0.,0., 
		    0.,1.,0.,0.,
		    0.,0.,1,0.,
		    0.,0.,0.,1.};
  int N = 4, nrhs = 4, lda = 4, ldb = 4;
  int ipiv[4];
  int info;
  dgesv_(&N, &nrhs, A, &lda, ipiv, RHS, &ldb, &info);
  assert(info>=0 && "HermiteShape: Could not invert matrix to find coefficients");
  
  // Save the coefficient matrix
  // Modify shape functions 2 and 3 based on prescribed slope
  double presval[] = {1.,1., 1., 1. };
  for(int a=0; a<4; a++)
    for(int i=0; i<4; i++)
      Coef[a][i] = RHS[4*i+a]*presval[a];
}


// Copy constructor
HermiteShape::HermiteShape(const HermiteShape & HShp)
  :Shape(HShp),
   elmcoord{HShp.elmcoord[0], HShp.elmcoord[1]}
{
  for(int a=0; a<4; a++)
    for(int i=0; i<4; i++)
      Coef[a][i] = HShp.Coef[a][i];
}

// Computes the value of requested shape function at given point.
double HermiteShape::Val(const int a,  const double *x) const
{
  double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]); 
  double val = 0.;
  for(int i=0; i<4; i++)
    val += Coef[a][i]*std::pow(y,double(i));
  return val;
}


// Computes the derivative of the requested shape function in the requested direction at given point.
double HermiteShape::DVal(const int a, const double *x, const int i) const
{
  assert(i==0 && "HermiteShape::DVal- Unexpected derivative number requested");
  double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]);

  // Derivative wrt cartesian coordinates
  double dval = Coef[a][1]*1.0 + Coef[a][2]*2.0*y + Coef[a][3]*3.0*std::pow(y,2.);

  // scaling to make derivative wrt barycentric coordinate.
  return (elmcoord[0]-elmcoord[1])*dval;   
}


// Computes the 2nd derivative
double HermiteShape::D2Val(const int a, const double* x) const
{
  double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]);

  // Derivative
  double d2val = Coef[a][2]*2. + Coef[a][3]*6.*y;

  // scaling to make derivative wrt barycentric coordinate.
  return (elmcoord[0]-elmcoord[1])*(elmcoord[0]-elmcoord[1])*d2val;   
}
  
