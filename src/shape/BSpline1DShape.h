// Sriramajayam

#ifndef BSPLINE_1D_SHAPE_H
#define BSPLINE_1D_SHAPE_H 

#include <Shape.h>
#include <vector>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_errno.h>

template<int DEGREE>
class BSpline1DShape: public Shape
{
 public:
  //! Constructor
  //! \param kv Knot vector, copied
  BSpline1DShape(const std::vector<double>& kv);
  
  //! Destructor
  virtual ~BSpline1DShape();

  //! Copy constructor
  //! \param Obj Object to be copied
  BSpline1DShape(const BSpline1DShape<DEGREE>& Obj);
  
  //! Cloning
  virtual BSpline1DShape<DEGREE>* Clone() const override;
  
  //! Returns the number of knots
  virtual int GetNumberOfKnots() const;
  
  //! Returns the requested knot
  //! \param k knot number
  virtual double GetKnot(const int k) const;
  
  //! Returns the knot vector
  virtual std::vector<double> GetKnotVector() const;
  
  //! Returns the degree of spline
  virtual int GetDegreeOfSpline() const;
  
  //! Returns the total number of (basis) shape functions
  virtual int GetNumberOfFunctions() const override;
  
  //! Returns the number of basis functions per interval
  virtual int GetNumberOfFunctionsPerInterval() const;
  
  //! Returns the number of active basis functions in given interval
  //! \param intnum Interval number
  virtual  int GetNumberOfFunctionsInInterval(const  int intnum) const;

  //! Returns the number of pieces (elements)
  virtual int GetNumberOfIntervals() const;

  //! Returns the limits of a requested interval
  //! \param intnum Interval number whose limits to return
  virtual std::vector<double> GetInterval(const  int intnum) const;
    
  //! Returns the indices of shape functions active on a given interval
  virtual std::vector<int> 
    GetActiveShapeFunctions(const  int intnum) const;

  //! Returns the knot number constituting the given interval
  //! \param intnum Interval number
  virtual std::vector<int> GetIntervalKnotNumbers(const  int intnum) const;
     
  //! Returns the number of parametric variables
  virtual int GetNumberOfVariables() const override;
  
  //! Evaluates the requested shape function at the given point
  //! \param a Shape function number
  //! \param x Parametric coordinate in the range of knot vectors
  virtual double Val(int a, const double *x) const override;
  
  //! Evalutates the derivatives of a shape function at the given point
  //! \param a Shape function number
  //! \param x Parametric coordinate in the range of knot vectors
  //! \param i Direction number, should be 0
  virtual double DVal(int a, const double *x, int i) const override;
  
  //! Evaluates the second derivative of a shape function at the given point
  //! \param a Shape function number
  //! \param x Parametric coordinate in the range of knot vectors
  //! \param i First direction, should be 0
  //! \param j Second direction, should be 0
  virtual double D2Val(int a, const double *x, int i, int j) const;
  
  //! Computes nonzero shape functions at given parametric coordinate and returns the
  //! shape function numbers
  //! \param xi Parametric coordinate
  //! \param vals Computed list of values for shape functions
  //! \param shpnum Numbers of nonzero shape functions
  virtual void GetNonzeroShapes(const double xi, 
				std::vector<int>& shpnum, 
				std::vector<double>& vals) const;
  
  //! Computes derivatives wrt parameteric coordinates of 
  //! nonzero shape functions at given parametric coordinate and 
  //! returns the shape function numbers
  //! \param xi Parametric coordinate
  //! \param dvals Computed list of values for shape functions
  //! \param shpnum Numbers of nonzero shape functions
  virtual void GetNonzeroDShapes(const double xi, 
				 std::vector<int>& shpnum, 
				 std::vector<double>& dvals) const;
  
  //! Computes 2nd derivatives wrt parametric coordinates of nonzero shape functions 
  //! at given parametric coordinate and 
  //! returns the shape function numbers
  //! \param xi Parametric coordinate
  //! \param d2vals Computed list of values for shape functions
  //! \param shpnum Numbers of nonzero shape functions
  virtual void GetNonzeroD2Shapes(const double xi, 
				  std::vector< int>& shpnum, 
				  std::vector<double>& d2vals) const;
  

  
 protected:
  //! Default constructor
  BSpline1DShape();
  
  //! Initialize spline when given the knot vector
  void InitializeBSpline(const std::vector<double>& knots);
  
  //! Returns the number of break points
  int GetNumberOfBreakPoints() const;
  
  //! Returns the order of the spline
  int GetOrderOfSpline() const;
  
  //! Workspace for b-spline
  gsl_bspline_workspace *bw;
};

//! Linear B-spline functions
using LinearBSpline1DShape = BSpline1DShape<1>;

//! Quadratic B-spline functions
using QuadraticBSpline1DShape = BSpline1DShape<2>;

//! Cubic B-spline functions
using CubicBSpline1DShape = BSpline1DShape<3>;

#endif

#include <BSpline1DShape_impl.h>
