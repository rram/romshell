// Sriramajayam

#ifndef BSPLINE_1D_ELEMENT_H
#define BSPLINE_1D_ELEMENT_H

#include <Element.h>
#include <SegmentNoCoordArray.h>
#include <BSpline1DShape.h>
#include <BasisFunctionsProvided.h>
#include <LineQuadratures.h>

template<int DEGREE, int NFields>
  class BSpline1DElement: public Element
{
 public:
  //! Constructor
  //! \param e Interval number in MyShp over which this element is 
  //! constructed
  //! \param MyShp BSpline shape functions, not copied
  BSpline1DElement(const int e, const BSpline1DShape<DEGREE> *MyShp);
  
  //! Destructor
  virtual ~BSpline1DElement();
  
  //! Copy constructor
  //! \param Obj Object to be copied
  BSpline1DElement(const BSpline1DElement<DEGREE, NFields>& Obj);
  
  //! Cloning
  virtual BSpline1DElement<DEGREE, NFields>* Clone() const override;
  
  //! Returns the BSpline shape functions object
  virtual const BSpline1DShape<DEGREE>* GetBSplineShapeFunctions() const;
  
  //! Returns the interval number of bspline function for this element
  virtual int GetBSplineIntervalNumber() const;

  //! Returns the element geometry
  const SegmentNoCoordArray<1>& GetElementGeometry() const override;
  
  //! Access to second derivatives of shape functions wrt
  //! Barycentric coordinates at quadrature points
  //! \param field Field number
  //! \param quad Quadrature point number
  //! \param shapenumber Local shape function number
  //! \param dir1 Direction 1 (always assumed to be 0)
  //! \param dir2 Direction 2 (always assumed to be 0)
  double GetD2Shape(const int field, const int quad, const int shapenumber, 
		    const int dir1, const int dir2) const;
  
  //! Evaluates shape function at given Barycentric coordinate
  double EvaluateShape(const double lambda, const int a) const;
  
  //! Evaluates derivative of shape function wrt Barycentic coordinate
  //! at given Barycentric coordinate
  double EvaluateDShape(const double lambda, const int a) const;
  
  //! Evaluates second derivative of shape function wrt Barycentric
  //! coordinates at given Barycentroc coordinate
  double EvaluateD2Shape(const double lambda, const int a) const;
  
 protected:
  //! Return the position in LocalShapes where shape functions for a 
  //! given field are saved.
  //! \param fieldnum Field number
  virtual int GetFieldIndex(int fieldnum) const override;
  
 private:
  const BSpline1DShape<DEGREE>* Shp;   //!< B-spline shape functions
  const int intervalnum;   //!< Interval number in splines object
  const SegmentNoCoordArray<1>* SegGeom;   //!< Geometry of this element
  const std::vector<double>* D2Shapes;   //!< Second derivative of shape functions at quadrature points
};

#endif

#include <BSpline1DElement_impl.h>
