// Sriramajayam

#include <CubicBSpline1DElement.h>
#include <iostream>

const  int NFields = 2;

void PrintL2GMapDetails(const L2GMapForCubicBSpline1DElements<NFields> &L2GMap);

int main()
{
  // Knot vector 
  int nknots = 12;
  std::vector<double> knotvector;
  {
    double kv[] = {-3,-2,-1,0,1,2,3,4,5,6,7,8};
    knotvector.assign( kv, kv+nknots );
  }
  
  // Create B spline shape functions
  CubicBSpline1DShape Shp(knotvector);

  // Create elements
  const  int nIntervals = Shp.GetNumberOfIntervals();
  
  std::vector<Element*> ElmArray(nIntervals);
  for( int e=0; e<nIntervals; e++)
    ElmArray[e] = new CubicBSpline1DElement<NFields>(e, &Shp);

  L2GMapForCubicBSpline1DElements<NFields>* Clone;
  {
    std::cout<<"\n\nTesting object: "
	     <<"\n===================";
    L2GMapForCubicBSpline1DElements<NFields> L2GMap(ElmArray);
    PrintL2GMapDetails(L2GMap);

    std::cout<<"\n\nTesting copy: "
	     <<"\n===================";
    L2GMapForCubicBSpline1DElements<NFields> Copy(L2GMap);
    PrintL2GMapDetails(Copy);
    
    Clone = L2GMap.Clone();
  }
  
  std::cout<<"\n\nTesting clone: "
	   <<"\n===================";
  PrintL2GMapDetails(*Clone);
  delete Clone;
  
  std::cout<<"\n\nDONE\n\n";
}


void PrintL2GMapDetails(const L2GMapForCubicBSpline1DElements<NFields> &L2GMap)
{
  std::cout<<"\nNumber of elements: "<<L2GMap.GetNumElements();
  std::cout<<"\nNumber of fields in each element: ";
  for( int e=0; e<L2GMap.GetNumElements(); e++)
    std::cout<<L2GMap.GetNumFields(e)<<" ";
  std::cout<<"\nNumber of dofs in each pair of fields in each element: ";
  for( int e=0; e<L2GMap.GetNumElements(); e++)
    {
      std::cout<<"("; 
      for( int f=0; f<L2GMap.GetNumFields(e); f++)
	std::cout<<L2GMap.GetNumDofs(e,f)<<", ";
      std::cout<<")  ";
    }
  std::cout<<"\nTotal number of dofs: "<<L2GMap.GetTotalNumDof();
  std::cout<<"\nLocal to global map: "
	   <<"\n----------------------";
  for( int e=0; e<L2GMap.GetNumElements(); e++)
    {
      std::cout<<"\nElement "<<e<<": ";
      for( int f=0; f<L2GMap.GetNumFields(e); f++)
	{
	  std::cout<<"field "<<f<<": (";
	  for(int a=0; a<L2GMap.GetNumDofs(e, f); a++)
	    std::cout<<L2GMap.Map(f,a,e)<<",";
	  std::cout<<")\t";
	}
    }
}
