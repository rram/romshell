// Sriramajayam

#include <QuadraticBSpline1DElement.h>
#include <iostream>
#include <fstream>

void PrintElementDetails(const QuadraticBSpline1DElement<1>& Elm);

int main()
{
    // Create quadratic b spline shape functions
  int nknots = 6;
  std::vector<double> knots; knots.clear();
  for(int k=0; k<nknots; k++)
    knots.push_back( 2.*double(k) );
  QuadraticBSpline1DShape Shp(knots);
  std::cout<<"\nNumber of global shape functions: "<<Shp.GetNumberOfFunctions();
  std::cout<<"\nNumber of intervals: "<<Shp.GetNumberOfIntervals();

  QuadraticBSpline1DElement<1>* Clone;
  {
    std::cout<<"\n\nTesting object: "
	     <<"\n==================";
    QuadraticBSpline1DElement<1> Elm(0, &Shp);
    PrintElementDetails(Elm);

    std::cout<<"\n\nTesting copy: "
	     <<"\n==================";
    QuadraticBSpline1DElement<1> Copy(Elm);
    PrintElementDetails(Copy);
    
    Clone = Elm.Clone();
  }
  
  std::cout<<"\n\nTesting clone: "
	   <<"\n==================";
  PrintElementDetails(*Clone);
  
  std::cout<<"\n\nDONE\n\n";
}


void PrintElementDetails(const QuadraticBSpline1DElement<1>& Elm) 
{
  std::cout<<"\nNumber of fields: "<<Elm.GetNumFields();
  std::cout<<"\nNumber of dofs: "<<Elm.GetDof(0);
  std::cout<<"\nNumber of derivatives: "<<Elm.GetNumDerivatives(0);
  std::cout<<"\nInterval number of element in spline: "<<Elm.GetBSplineIntervalNumber();
  std::cout<<"\nDegree of spline in element: "<<Elm.GetBSplineShapeFunctions()->GetDegreeOfSpline();
  int nquad = int(Elm.GetIntegrationWeights(0).size());
  std::cout<<"\nNumber of integration points: "<<nquad;
  std::cout<<"\nCoordinates of integration points: ";
  for(int q=0; q<nquad; q++)
    std::cout<<Elm.GetIntegrationPointCoordinates(0)[q]<<"  ";
  std::cout<<"\nShape function values: "
	   <<"\n-------------------------";
  for(int q=0; q<nquad; q++)
    {
      std::cout<<"\n";
      for(int a=0; a<Elm.GetDof(0); a++)
	std::cout<<Elm.GetShape(0, q, a)<<"  ";
    }
  std::cout<<"\nDerivatives of shape functions: "
	   <<"\n--------------------------------";
  for(int q=0; q<nquad; q++)
    {
      std::cout<<"\n";
      for(int a=0; a<Elm.GetDof(0); a++)
	std::cout<<Elm.GetDShape(0, q, a, 0)<<"  ";
    }
  std::cout<<"\nSecond derivatives of shape functions: "
	   <<"\n----------------------------------------";
  for(int q=0; q<nquad; q++)
    {
      std::cout<<"\n";
      for(int a=0; a<Elm.GetDof(0); a++)
	std::cout<<Elm.GetD2Shape(0, q, a, 0, 0)<<"  ";
    }
  
  std::cout<<"\nPerforming checks for partition of unity and consistency of derivatives: "<<std::flush;
  {  
    const double EPS = 1.e-5;
    const int N = 10;
    for(int i=1; i<N; i++)
      {
	double lambda = double(i)/double(N);
	double lambda_plus = lambda+EPS;
	double lambda_minus = lambda-EPS;
	
	// Check that shape functions add to 1
	double shpsum = 0.;
	
	for(int a=0; a<Elm.GetDof(0); a++)
	  {
	    double xi; Elm.GetElementGeometry().Map(&lambda, &xi);
	    double shp = Elm.EvaluateShape(lambda, a);
	    shpsum += shp;
	    double dshp = Elm.EvaluateDShape(lambda, a);
	    double d2shp = Elm.EvaluateD2Shape(lambda, a);
	    
	    double xiplus; Elm.GetElementGeometry().Map(&lambda_plus, &xiplus);
	    double shp_plus = Elm.EvaluateShape(lambda+EPS, a);
	    
	    double ximinus; Elm.GetElementGeometry().Map(&lambda_minus, &ximinus);
	    double shp_minus = Elm.EvaluateShape(lambda_minus, a);
	    
	    double dshp_num = (shp_plus-shp_minus)/(xiplus-ximinus);
	    double d2shp_num = (shp_plus-2.*shp+shp_minus)/((xiplus-xi)*(xi-ximinus));
	    
	    //if(std::abs(dshp-dshp_num)>EPS*std::abs(dshp))
	    //{ std::cout<<"\n\n"<<dshp<<" should be close to "
	    //		 <<dshp_num<<"\n\n"<<std::flush;  exit(1);}

	    assert(std::abs(dshp-dshp_num)<EPS && "Derivative check failed");
	    
	    assert(std::abs(d2shp-d2shp_num)<EPS && "2nd derivative check failed");
	  }
	assert(std::abs(shpsum-1.)<EPS && "Partition of unity failed");
      }
    std::cout<<"verified.";
  }
}
