// Sriramajayam

#include <HermiteElement.h>
#include <iostream>
#include <cassert>

const int nFields = 3;

void TestElement(const Element& Elm);

int main()
{
  std::vector<double> coordinates({std::sqrt(0.), std::sqrt(1.)});
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  HermiteElement<nFields> Elm(1,2);
  TestElement(Elm);
}


void TestElement(const Element& Elm)
{
  assert(Elm.GetNumFields()==nFields);
  for(int f=0; f<nFields; ++f)
    {
      assert(Elm.GetDof(f)==4);
      assert(Elm.GetNumDerivatives(f)==1);
      const int nQuad = static_cast<int>(Elm.GetIntegrationWeights(f).size());
      assert(nQuad==8);
      double qsum = 0.;
      for(int q=0; q<nQuad; ++q)
	{
	  const double xq = Elm.GetIntegrationPointCoordinates(f)[0];
	  assert(xq>=std::sqrt(0.) && xq<=std::sqrt(1.));
	  qsum += Elm.GetIntegrationWeights(f)[q];
	  for(int a=0; a<4; ++a)
	    std::cout<<"\n"<<Elm.GetDShape(0,q,a,0);
	}
      assert(std::abs(qsum+std::sqrt(0.)-std::sqrt(1.))<1.e-5);
    }
  // done
  return;
}
  
