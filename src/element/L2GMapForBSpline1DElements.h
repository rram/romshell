// Sriramajayam

#ifndef L2GMAP_FOR_BSPLINE_1D_ELEMENTS_H
#define L2GMAP_FOR_BSPLINE_1D_ELEMENTS_H

#include <Element.h>
#include <LocalToGlobalMap.h>
#include <BSpline1DElement.h>
#include <cassert>

template<int DEGREE, int NFields>
  class L2GMapForBSpline1DElements: public LocalToGlobalMap
{
 public:
  //! Constructor
  //! \param EA Vector of elements, should be type-castable to 
  //! BSpline1DElement<DEGREE, NFields>.
  //! Pointed to, not copied
  inline L2GMapForBSpline1DElements(const std::vector<Element *> &EA)
    :LocalToGlobalMap(), ElmArray(&EA)
    {
      // Some sanity checks
      assert(EA[0]->GetNumFields()==NFields &&
	     "L2GMapForBSpline1DElements: inconsistent number of fields");

      const BSpline1DElement<DEGREE, NFields>* MyElm = 
	dynamic_cast<const BSpline1DElement<DEGREE, NFields>* >(EA[0]);
      assert(MyElm!=nullptr &&
	     "L2GMapForBSpline1DElements: Could not cast element to type BSpline1DElement.");
    }
  
  //! Destructor
  inline virtual ~L2GMapForBSpline1DElements() {}
  
  //! Copy constructor
  //! \param Obj Object to be copied
  inline L2GMapForBSpline1DElements(const L2GMapForBSpline1DElements<DEGREE, NFields> &Obj)
    :LocalToGlobalMap(Obj), ElmArray(Obj.ElmArray)
    {}
  
  //! Cloning
  inline virtual L2GMapForBSpline1DElements<DEGREE, NFields>* Clone() const override
    { return new L2GMapForBSpline1DElements<DEGREE, NFields>(*this); }

  //! Return the total number of elements
  inline virtual int GetNumElements() const override
  { return static_cast<int>(ElmArray->size()); }

  //! Return the number of fields in a given element
  //! \param elm Element number
  virtual int GetNumFields(const int elm) const override
  { return NFields; }
    
  //! Returns the number of dofs in given field in a given element
  //! \param elm ElementNumber
  //! \param field Field number
  inline virtual int GetNumDofs(const int elm, const int field) const override
  { return (*ElmArray)[elm]->GetDof(field); }

  //! Returns the total number of dofs
  inline virtual int GetTotalNumDof() const override
  {
    const BSpline1DElement<DEGREE, NFields>* Elm0 = 
      dynamic_cast<const BSpline1DElement<DEGREE, NFields>* >((*ElmArray)[0]);
    assert(Elm0!=nullptr &&
	   "L2GMapForBSpline1DElements::GetTotalNumDof()- Could not type cast element");

    return NFields*Elm0->GetBSplineShapeFunctions()->GetNumberOfFunctions();
  }

  //! Main functionality- map local dof of field to global dof 
  //! \param field Field number
  //! \param dof Local dof number 
  //! \param ElementMapped Element number
  inline int Map(const int field, const int dof, const int ElementMapped) const override
  {
    // Get the shape function number dual to dof 
    const BSpline1DElement<DEGREE, NFields>* Elm = 
      dynamic_cast<const BSpline1DElement<DEGREE, NFields>* >((*ElmArray)[ElementMapped]);
    assert(Elm!=nullptr && "L2GMapForBSpline1DElements::Map: Could not typecast element to BSpline1DElement");
    int intervalnum = Elm->GetBSplineIntervalNumber();
    int shpnum = Elm->GetBSplineShapeFunctions()->GetActiveShapeFunctions(intervalnum)[dof];
    return NFields*shpnum + field;
  }
    
 protected:
  //! Provide access to the element array for derived classes
  inline const std::vector<Element*>& GetElementArray() const
  { return *ElmArray; }
  
 private:
  //! Pointer to element array
  const std::vector<Element*>* ElmArray;
};

#endif
