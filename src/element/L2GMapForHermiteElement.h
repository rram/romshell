// Sriramajayam

#ifndef L2GMAP_FOR_HERMITE_ELEMENT
#define L2GMAP_FOR_HERMITE_ELEMENT

#include <P12DElement.h>

class L2GMapForHermiteElement: public LocalToGlobalMap
{
 public:
  //! Constructor
  //! \param EA Element array
  inline L2GMapForHermiteElement(const std::vector<Element*>& EA)
    :LocalToGlobalMap(),
    P11DMap(EA),
    nNodes(P11DMap.GetTotalNumDof()/P11DMap.GetNumFields(0))
      {}
    
  //! Destructor
  inline virtual ~L2GMapForHermiteElement() {}

  //! Copy constructor
  //! \param[in] obj Object to be copied from
  inline L2GMapForHermiteElement(const L2GMapForHermiteElement& Obj)
    :LocalToGlobalMap(Obj),
    P11DMap(Obj.P11DMap),
    nNodes(Obj.nNodes)
      {}

  //! Clone
  inline virtual L2GMapForHermiteElement* Clone() const
  { return new L2GMapForHermiteElement(*this); }

  //! Main functionality: Map dofs 0, 1 consecutively. Then map dofs 2 and 3.
  inline virtual int Map(int field, const int dof, const int elm) const override
  {
    if(dof==0 || dof==1)
      return P11DMap.Map(field, dof, elm);
    else
      return P11DMap.GetNumFields(elm)*nNodes + P11DMap.Map(field, dof-2, elm);
  }

  inline virtual int GetNumElements() const override
  { return P11DMap.GetNumElements(); }

  inline virtual int GetNumFields(const int elm) const override
  { return P11DMap.GetNumFields(elm); }
  
  inline virtual int GetNumDofs(const int elm, const int field) const override
  { return P11DMap.GetNumDofs(elm, field); }
  
  //! Total number of dofs
  inline int GetTotalNumDof() const
  { return 2*P11DMap.GetTotalNumDof(); }

 private:
  const StandardP12DMap P11DMap;
  const int nNodes;
};

#endif
