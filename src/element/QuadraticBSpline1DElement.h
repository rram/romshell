// Sriramajayam

#ifndef QUADRATIC_BSPLINE_1D_ELEMENT_H
#define QUADRATIC_BSPLINE_1D_ELEMENT_H

#include <BSpline1DElement.h>
#include <L2GMapForBSpline1DElements.h>

template<int NFields>
class QuadraticBSpline1DElement: public BSpline1DElement<2, NFields>
{
 public:
  //! Constructor
  //! \param e Interval number in MyShp over which this element is 
  //! constructed
  //! \param MyShp BSpline shape functions, not copied
  inline QuadraticBSpline1DElement(const int e, 
				   const QuadraticBSpline1DShape *MyShp)
    :BSpline1DElement<2, NFields>(e, MyShp) {}
  
  //! Destructor
  inline virtual ~QuadraticBSpline1DElement() {}
  
  
  //! Copy constructor
  //! \param Obj Object to be copied
  inline QuadraticBSpline1DElement(const QuadraticBSpline1DElement<NFields>& Obj)
    :BSpline1DElement<2, NFields>(Obj) {}
  
  //! Cloning
  inline virtual QuadraticBSpline1DElement<NFields>* Clone() const override
  { return new QuadraticBSpline1DElement<NFields>(*this); }
 
};


template<int NFields>
class L2GMapForQuadraticBSpline1DElements: public L2GMapForBSpline1DElements<2, NFields>
{
 public:
  //! Constructor
  //! \param EA Vector of elements, should be type-castable to 
  //! BSpline1DElement<DEGREE, NFields>.
  //! Pointed to, not copied
  inline L2GMapForQuadraticBSpline1DElements(const std::vector<Element*> &EA)
    :L2GMapForBSpline1DElements<2, NFields>(EA) {}
  
  //! Destructor
  inline virtual ~L2GMapForQuadraticBSpline1DElements() {}
  
  //! Copy constructor
  //! \param Obj Object to be copied
  inline L2GMapForQuadraticBSpline1DElements
    (const L2GMapForQuadraticBSpline1DElements<NFields> &Obj)
    :L2GMapForBSpline1DElements<2, NFields>(Obj) {}
  
  //! Cloning
  inline virtual L2GMapForQuadraticBSpline1DElements<NFields>* Clone() const override
    { return new L2GMapForQuadraticBSpline1DElements<NFields>(*this); }
  
};
#endif
