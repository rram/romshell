// Sriramajayam

#ifndef BSPLINE_1D_ELEMENT_IMPL_H
#define BSPLINE_1D_ELEMENT_IMPL_H

#include <BSpline1DElement.h>
#include <cassert>

// Implementation

// Constructor
template<int DEGREE, int NFields>
  BSpline1DElement<DEGREE, NFields>::
  BSpline1DElement(const  int e, 
		   const BSpline1DShape<DEGREE> *MyShp)
  :Element(), Shp(MyShp), intervalnum(e)
{
  // Create geometry
  const std::vector<double> tinterval = MyShp->GetInterval(e);
  SegGeom = new SegmentNoCoordArray<1>(-1, -1, &tinterval[0]);
  
  // Quadrature rule
  const Quadrature* QD = Line_8pt::Bulk;
  const int nquad = QD->GetNumberQuadraturePoints();
  
  // Evaluate quadrature rules, shape functions, derivatives
  std::vector<double> Qpts; Qpts.clear();
  std::vector<double> Qwts; Qwts.clear();
  std::vector<double> Shapes; Shapes.clear();
  std::vector<double> DShapes; DShapes.clear();
  std::vector<double> MyD2Shapes; MyD2Shapes.clear();
  
  for(int q=0; q<nquad; q++)
    {
      double lambda = QD->GetQuadraturePoint(q)[0];
      
      // Quadrature point
      double X;
      SegGeom->Map(&lambda, &X);
      Qpts.push_back( X );
      
      // Quadrature weight
      double DX, Jac;
      SegGeom->DMap(&lambda, &DX, Jac);
      Qwts.push_back( QD->GetQuadratureWeights(q)*Jac );
      
      // Shape functions and derivatives
      std::vector<int> shpnum;
      std::vector<double> shpval, dshpval, d2shpval;
      MyShp->GetNonzeroShapes(X, shpnum, shpval);
      MyShp->GetNonzeroDShapes(X, shpnum, dshpval);
      MyShp->GetNonzeroD2Shapes(X, shpnum, d2shpval);
      for(int i=0; i<static_cast<int>(shpnum.size()); i++)
	{
	  Shapes.push_back( shpval[i] );
	  DShapes.push_back( dshpval[i] );
	  MyD2Shapes.push_back( d2shpval[i] );
	}
    }
  BasisFunctionsProvided ShpEval(Shapes, DShapes, Qwts, Qpts);
  AddBasisFunctions( ShpEval );
  D2Shapes = new std::vector<double>(MyD2Shapes);
  for(int i=0; i<NFields; ++i)
    AppendField(i);
}

// Destructor
template<int DEGREE,  int NFields>
  BSpline1DElement<DEGREE, NFields>::~BSpline1DElement()
{ 
  delete SegGeom; 
  delete D2Shapes;
}

// Copy constructor
template<int DEGREE, int NFields>
  BSpline1DElement<DEGREE, NFields>::
  BSpline1DElement(const BSpline1DElement<DEGREE, NFields>& Obj)
  :Element(Obj), Shp(Obj.Shp), intervalnum(Obj.intervalnum)
{
  SegGeom = Obj.SegGeom->Clone();
  D2Shapes = new std::vector<double>(*Obj.D2Shapes);
}

// Cloning
template<int DEGREE, int NFields>
  BSpline1DElement<DEGREE, NFields>* 
  BSpline1DElement<DEGREE, NFields>::Clone() const
{ return new BSpline1DElement<DEGREE, NFields>(*this); }


// Returns the BSpline shape functions object
template<int DEGREE, int NFields>
  const BSpline1DShape<DEGREE>* 
  BSpline1DElement<DEGREE, NFields>::GetBSplineShapeFunctions() const
{ return Shp; }
  
// Returns the element geometry
template<int DEGREE, int NFields>
  const SegmentNoCoordArray<1>&
  BSpline1DElement<DEGREE, NFields>::GetElementGeometry() const
{ return *SegGeom; }
  
// Access to second derivatives of shape functions wrt
// element coordinates at quadrature points
template<int DEGREE, int NFields>
  double BSpline1DElement<DEGREE, NFields>::
  GetD2Shape(const int field, const int quad, const int shapenumber, 
	     const int dir1, const int dir2) const
{ 
  const int nDof = GetDof(field);  
  return (*D2Shapes)[quad*nDof+shapenumber];
}
  
// Evaluates shape function at given Barycentric coordinate
template<int DEGREE, int NFields>
  double BSpline1DElement<DEGREE, NFields>::
  EvaluateShape(const double lambda, const int a) const
{
  double xi = 0.;
  SegGeom->Map(&lambda, &xi);
  int shpnum = Shp->GetActiveShapeFunctions(intervalnum)[a];
  return Shp->Val(shpnum, &xi);
}
  
//! Evaluates derivative of shape function wrt element coordinates
//! at given Barycentric coordinate
template<int DEGREE, int NFields>
  double BSpline1DElement<DEGREE, NFields>::
  EvaluateDShape(const double lambda, const int a) const
{
  double xi = 0.;
  SegGeom->Map(&lambda, &xi);
  int shpnum = Shp->GetActiveShapeFunctions(intervalnum)[a];
  return Shp->DVal(shpnum, &xi, 0);
}

// Evaluates second derivative of shape function wrt element coordinates
// at given Barycentric coordinate
template<int DEGREE, int NFields>
  double BSpline1DElement<DEGREE, NFields>::
  EvaluateD2Shape(const double lambda, const int a) const
{ 
  double xi = 0.;
  SegGeom->Map(&lambda, &xi);
  int shpnum = Shp->GetActiveShapeFunctions(intervalnum)[a];
  return Shp->D2Val(shpnum, &xi, 0, 0);
}

// Return the position in LocalShapes where shape functions for a 
// given field are saved.
template<int DEGREE, int NFields>
  int BSpline1DElement<DEGREE, NFields>::
  GetFieldIndex(int fieldnum) const
{ return 0; }
  
// Returns the interval number of bspline function for this element
template<int DEGREE, int NFields>
  int BSpline1DElement<DEGREE, NFields>::GetBSplineIntervalNumber() const
{ return intervalnum; }


#endif
