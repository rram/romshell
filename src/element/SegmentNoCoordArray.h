// Sriramajayam

#ifndef SEGMENTNOCOORDARRAY
#define SEGMENTNOCOORDARRAY

#include "Segment.h"

//! \brief Class for a segment but without a global coordinates array.
/** This is the class for ElementGeometry Segment but when a global coordinates
 * array is not provided. Hence, the coordinates for the geometry are not
 * provided by access to a coordinates array using the connectivity.
 * Instead, the coordinates of each geometry is copied.
 */

template<int spatial_dimension>
class SegmentNoCoordArray: public Segment<spatial_dimension>
{
 public:
  
  //! Constructor
  //! \param i1 Node number for node 1.
  //! \param i2 Node number for node 2.
  //! \param ecoord Coordinates for nodes 1 and 2. Has length 2*spatial_dimension
  //! \warning May still have to set the global coordinates array of Segment.
  SegmentNoCoordArray(const int i1, const int i2, 
		      const double * ecoord)
    :Segment<spatial_dimension>(i1, i2)
    {
      for(int a=0; a<2; a++)
	for(int k=0; k<spatial_dimension; k++)
	  elmcoord[a*spatial_dimension+k] = ecoord[a*spatial_dimension+k];
    }
  
  //! Destructor
  inline virtual ~SegmentNoCoordArray() {}
  
  //! Copy constructor
  inline SegmentNoCoordArray(const SegmentNoCoordArray& OldS)
    :Segment<spatial_dimension>(OldS)
    { 
      for(int a=0; a<2; a++)
	for(int k=0; k<spatial_dimension; k++)
	  elmcoord[a*spatial_dimension+k] = OldS.elmcoord[a*spatial_dimension+k];
    }
  
  //! Cloning
  SegmentNoCoordArray * Clone() const
    { return new SegmentNoCoordArray<spatial_dimension>(*this); }
  
  //! Returns name of element geometry
  inline std::string GetPolytopeName() const
    { return "SegmentNoCoordinatesArray"; }
  
 protected:
  //! Returns the coordinate of nodes.
  //! \param a Local node number
  //! \param i coordinate direction number.
  double GetCoordinate(int a, int i) const
    { return elmcoord[a*spatial_dimension+i]; }
  
 private:
  double elmcoord[2*spatial_dimension];
};


#endif
