// Sriramajayam

#ifndef CUBIC_BSPLINE_1D_ELEMENT_H
#define CUBIC_BSPLINE_1D_ELEMENT_H

#include <BSpline1DElement.h>
#include <L2GMapForBSpline1DElements.h>

template<int NFields>
class CubicBSpline1DElement: public BSpline1DElement<3, NFields>
{
 public:
  //! Constructor
  //! \param e Interval number in MyShp over which this element is 
  //! constructed
  //! \param MyShp BSpline shape functions, not copied
  inline CubicBSpline1DElement(const int e, 
			       const CubicBSpline1DShape *MyShp)
    :BSpline1DElement<3, NFields>(e, MyShp) {}
  
  //! Destructor
  inline virtual ~CubicBSpline1DElement() {}
  
  //! Copy constructor
  //! \param Obj Object to be copied
  inline CubicBSpline1DElement(const CubicBSpline1DElement<NFields>& Obj)
    :BSpline1DElement<3, NFields>(Obj) {}
  
  //! Cloning
  inline virtual CubicBSpline1DElement<NFields>* Clone() const override
  { return new CubicBSpline1DElement<NFields>(*this); }
 
};



template<int NFields>
class L2GMapForCubicBSpline1DElements: public L2GMapForBSpline1DElements<3, NFields>
{
 public:
  //! Constructor
  //! \param EA Vector of elements, should be type-castable to 
  //! BSpline1DElement<DEGREE, NFields>.
  //! Pointed to, not copied
  inline L2GMapForCubicBSpline1DElements(const std::vector<Element *> &EA)
    :L2GMapForBSpline1DElements<3, NFields>(EA) {}
  
  //! Destructor
  inline virtual ~L2GMapForCubicBSpline1DElements() {}
  
  //! Copy constructor
  //! \param Obj Object to be copied
  inline L2GMapForCubicBSpline1DElements
    (const L2GMapForCubicBSpline1DElements<NFields> &Obj)
    :L2GMapForBSpline1DElements<3, NFields>(Obj) {}
  
  //! Cloning
  inline virtual L2GMapForCubicBSpline1DElements<NFields>* Clone() const override
    { return new L2GMapForCubicBSpline1DElements<NFields>(*this); }
  
};
#endif
